namespace detail {

template <class> class scope_guard;
template <class T> scope_guard<T> make_scope_guard(T);
    
struct forward_tag {};
    
template <class T> class scope_guard {
    T _freeFn;
    mutable bool _ignore = false;

public:    
    scope_guard(const scope_guard&) = delete;

    scope_guard(scope_guard&& rhs) noexcept : scope_guard() {
        swap(rhs);
    }
    
    scope_guard& operator=(scope_guard rhs) noexcept {
        swap(rhs);
        return *this;
    }
    
    ~scope_guard() try {
        if (not _ignore) {
            _freeFn();
        }
    }
    catch (...) {
    }
    
    void dismiss() const {
        _ignore = true;
    }
    
private:
    scope_guard(forward_tag, T freeFn) : _freeFn(std::move(freeFn)) {
    }
    
    void swap(scope_guard& rhs) {
        std::swap(_freeFn, rhs._freeFn);
    }
    
friend scope_guard make_scope_guard<T>(T);
};
    
template <class T> scope_guard<T> make_scope_guard(T freeFn) {
    return scope_guard<T>(forward_tag{}, std::move(freeFn));
}

} // namespace detail

#define scope_guard(Var, Body) const auto& Var = detail::make_scope_guard([&]Body); Var
