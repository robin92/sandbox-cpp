#include <iostream>
#include "scope_guard.hpp"

int main(int, char**) {
    scope_guard(grd, { std::cout << "hello again!\n"; });
    int x = 0;
    {
        scope_guard(valueChangeGrd, { x = __LINE__; });
    }
    std::cout << x << "\n";
    return 0;
}
