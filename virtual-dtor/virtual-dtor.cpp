#include <iostream>
#include <memory>

struct GoodBase {
    virtual ~GoodBase() { std::cout << "GoodBase dtor\n"; }
};

struct Foo : public GoodBase {
    ~Foo() { std::cout << "Foo dtor\n"; }
};

struct FurtherFoo : public Foo {
    ~FurtherFoo() { std::cout << "FurtherFoo dtor\n"; }
};

struct BadBase {
    ~BadBase() { std::cout << "BadBase dtor\n"; }
};

struct Bar : public BadBase {
    ~Bar() { std::cout << "Bar dtor\n"; }
};

struct FurtherBar : public Bar {
    ~FurtherBar() { std::cout << "FurtherBar dtor\n"; }
};

#define SCOPED(Type, Class) {\
    std::unique_ptr<Type> tmp = std::make_unique<Class>();\
}\
std::cout << "\n"

int main(int, char**) {
    SCOPED(Foo, Foo);
    SCOPED(Bar, Bar);

    SCOPED(FurtherFoo, FurtherFoo);
    SCOPED(FurtherBar, FurtherBar);

    SCOPED(GoodBase, Foo);
    SCOPED(BadBase, Bar);   // memory leak: Bar dtor not called

    SCOPED(GoodBase, FurtherFoo);
    SCOPED(BadBase, FurtherBar);    // memory leak: FurtherBar & Bar dtors not called
    return 0;
}

#undef SCOPED
