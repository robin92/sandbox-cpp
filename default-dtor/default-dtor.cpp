#include <iostream>
#include <type_traits>

struct A {
    ~A() = default;
};

struct B {
    ~B() {}
};

struct C {
    ~C();
};

C::~C() = default;

int main(int, char**) {
    std::cout << std::boolalpha
        << std::is_trivially_destructible<A>::value << "\n"
        << std::is_trivially_destructible<B>::value << "\n"
        << std::is_trivially_destructible<C>::value << "\n";
    return 0;
}
