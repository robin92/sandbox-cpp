#pragma once
#include <cstddef>
#include "intseq.hpp"

/// Wrapper for compile-time strings.
template <int N> class static_string {
    const char (&_data)[N + 1];

public:
    constexpr static_string(const char (&data)[N + 1]) : _data(data) {}

    constexpr operator const char*() const { return _data; }

    constexpr char operator[] (int i) const { return _data[i]; }
    constexpr size_t size() const { return N; }
    constexpr const char* c_str() const { return _data; }
};

/// Wrapper for storing character sequences as null-terminated array of chars.
template <int N> class char_array {
    // can't use std::array due to lacking constexpr
    char _data[N + 1] = {};

    template <int... PACK>
        constexpr char_array(static_string<N> rhs, intseq<PACK...>) : _data{rhs[PACK]..., '\0'} {}

    template <int NL, int... LHS_PACK, int... RHS_PACK>
        constexpr char_array(
                const char_array<NL>& lhs,
                const char_array<N - NL>& rhs,
                intseq<LHS_PACK...>,
                intseq<RHS_PACK...>) :
            _data{lhs[LHS_PACK]..., rhs[RHS_PACK]..., '\0'} {}

public:
    constexpr char_array(static_string<N> rhs) : char_array(rhs, make_intseq_t<N>{}) {}

    template <int NL>
        constexpr char_array(char_array<NL> lhs, char_array<N - NL> rhs) :
            char_array(lhs, rhs, make_intseq_t<NL>{}, make_intseq_t<N - NL>{}) {}

    constexpr operator const char*() const { return _data; }

    constexpr char operator[] (int i) const { return _data[i]; }
    constexpr size_t size() const { return N; }
    constexpr const char* c_str() const { return _data; }
};


// TODO generalize operator+ implementation so that only one function is needed
template <int NL, int NR>
constexpr char_array<NL + NR> operator+ (const char_array<NL>& lhs, const char_array<NR>& rhs) {
    return {lhs, rhs};
}


// static_string operator+
template <int NL, int NR>
constexpr char_array<NL + NR> operator+ (static_string<NL> lhs, static_string<NR> rhs) {
    return char_array<NL>(lhs) + char_array<NR>(rhs);
}

template <int NL, int NR>
constexpr char_array<NL + NR> operator+ (const char_array<NL>& lhs, static_string<NR> rhs) {
    return lhs + char_array<NR>(rhs);
}

template <int NL, int NR>
constexpr char_array<NL + NR> operator+ (static_string<NL> lhs, const char_array<NR> rhs) {
    return char_array<NL>(lhs) + rhs;
}


// can't use N as template parameter due to deduction rules
template <int SizePlusOne>
constexpr static_string<SizePlusOne - 1> literal(const char (&data)[SizePlusOne]) {
    return static_string<SizePlusOne - 1>(data);
}
