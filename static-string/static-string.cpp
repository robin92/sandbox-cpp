#include <cassert>
#include <cstring>
#include <iostream>
#include "staticstring.hpp"

void onlyCStr(const char* cstr) {
    std::cerr << cstr << "\n";
}

int main(int, char**) {
    constexpr auto s1 = literal("hello");
    constexpr char_array<5> arr = s1;
    std::cerr << arr << "\n";

    constexpr auto s2 = literal(" ");
    constexpr auto s3 = literal("world");
    constexpr auto s = s1 + s2 + s3;
    std::cerr << s << "\n";

    onlyCStr(s);
    std::string str(s);
    std::cerr << str << "\n";

    return 0;
}
