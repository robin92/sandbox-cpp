#pragma once

/// Stores sequence of ints that can be used in parameter packs.
//     intseq<0, 1>{}
//     intseq<0, 1, 2, 3>{}
//     intseq<17, 1, 24, 312>{}
template <int... Ns> struct intseq {};

namespace detail {

template <class T> struct append;

template <int... Is> struct append<intseq<Is...>> {
    // at the last use when instantiated for make_intseq_t<N>:
    //   Is = 0, 1, 2, ..., N-1
    //   sizeof...(Is) = N
    // so type will be
    //   intseq<0, 1, 2, ..., N-1, N>
    // also note that this member will be shadowed for each recursion step
    using type = intseq<Is..., sizeof...(Is)>;
};

// using curiously recurring template parameter
template <int N> struct make_intseq : append<typename make_intseq<N - 1>::type> {};

template <> struct make_intseq<0> {
    using type = intseq<>;
};

}  // namespace detail

/// Creates a sequence of integers such that
///   make_intseq_t<0> => intseq<>
///   make_intseq_t<1> => intseq<0>
///   make_intseq_t<2> => intseq<0, 1>
///   make_intseq_t<3> => intseq<0, 1, 2>
template <int N> using make_intseq_t = typename detail::make_intseq<N>::type;
