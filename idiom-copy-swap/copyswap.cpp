#include <iostream>

struct Foo {
    int a, b, c;

    Foo(int v) : a(v), b(v+100), c(v+1000) { std::cout << "ctor\n"; }
    Foo(const Foo& rhs) : a(rhs.a), b(rhs.b), c(rhs.c) { std::cout << "copy ctor\n"; } 
    ~Foo() { std::cout << "dtor\n"; }

    Foo& operator= (Foo rhs) {
        std::cout << "copy assign\n";
        swap(*this, rhs);
        return *this;
    }

friend void swap(Foo& lhs, Foo& rhs) noexcept {
    using std::swap;
    swap(lhs.a, rhs.a);
    swap(lhs.b, rhs.b);
    swap(lhs.c, rhs.c);
}
};

int main(int, char**) {
    Foo a(1), b(2);
    a = b;
    b = Foo(3);
    return 0;
}
