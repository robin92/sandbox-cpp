#include <iostream>
#include <memory>
#include <vector>

namespace ext {

class Sheep {
    void process() = delete;

public:
    void say() const {
        std::cout << "I'm a sheep!\n";
    }
};

}

class Foo {};
class Bar {};

class Base {
    struct Concept {
        virtual ~Concept() = default;
        virtual void process() = 0;
    };

    struct FooConcept : public Concept {
        void process() override {
            std::cout << "foo\n";
        }
    };

    struct BarConcept : public Concept {
        void process() override {
            std::cout << "Bar\n";
        }
    };

    class SheepConcept : public Concept {
        ext::Sheep _sheep;

    public:
        SheepConcept(ext::Sheep sheep) : _sheep(sheep) {}

        void process() override {
            _sheep.say();
        }
    };

    std::unique_ptr<Concept> _self;

public:
    Base(Foo) : _self(std::make_unique<FooConcept>()) {}
    Base(Bar) : _self(std::make_unique<BarConcept>()) {}
    Base(ext::Sheep s) : _self(std::make_unique<SheepConcept>(std::move(s))) {}

    void process() {
        _self->process();
    }
};

void process(std::vector<Base>& elements) {
    for (auto&& elem : elements) {
        elem.process();
    }
}

int main(int, char**) {
    std::vector<Base> data;
    data.push_back(Foo{});
    data.push_back(Foo{});
    data.push_back(Bar{});
    data.push_back(ext::Sheep{});
    process(data);
    return 0;
}
