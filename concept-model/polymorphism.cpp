#include <iostream>
#include <memory>
#include <vector>

namespace ext {

class Sheep {
    void process() = delete;

public:
    void say() const {
        std::cout << "I'm a sheep!\n";
    }
};

}

class Base {
public:
    virtual ~Base() = default;
    virtual void process() = 0;
};

void process(const std::vector<std::unique_ptr<Base>>& elements) {
    for (auto&& elem : elements) {
        elem->process();
    }
}

class Foo : public Base {
    void process() override {
        std::cout << "foo\n";
    }
};

class Bar : public Base {
    void process() override {
        std::cout << "bar\n";
    }
};

class Sheep : public Base, public ext::Sheep {
    void process() override {
        say();
    }
};

int main(int, char**) {
    std::vector<std::unique_ptr<Base>> data;
    data.push_back(std::make_unique<Foo>());
    data.push_back(std::make_unique<Foo>());
    data.push_back(std::make_unique<Bar>());
    data.push_back(std::make_unique<Sheep>());
    process(data);
    return 0;
}
