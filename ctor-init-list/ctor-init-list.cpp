#include <future>
#include <iostream>

struct Bar
{
    Bar(int i)
    {
        if (i % 2 == 0)
        {
            throw std::runtime_error("bar " + std::to_string(i));
        }
        std::cerr << "bar ctor " << this << "\n";
    }

    ~Bar()
    {
        std::cerr << "bar dtor " << this << "\n";
    }
};

class Foo
{
    Bar a, b, c, d;

public:
    Foo() : a(1), b(3), c(2), d(5)
    {
        std::cerr << "foo ctor\n";
    }

    ~Foo()
    {
        std::cerr << "foo dtor\n";
    }
};

int main(int, char**)
{
    std::async(std::launch::async, []{ Foo foo; });
    return 0;
}
