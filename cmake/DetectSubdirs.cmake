function(detect_subdirs)
    file(
        GLOB_RECURSE
        subprojects
        RELATIVE ${CMAKE_SOURCE_DIR}
        */CMakeLists.txt
    )
    list(SORT subprojects)

    foreach(subproject ${subprojects})
        get_filename_component(name ${subproject} DIRECTORY)
        message(STATUS "Configuring build system for ${name}")
        add_subdirectory(${name})
    endforeach()
endfunction()
