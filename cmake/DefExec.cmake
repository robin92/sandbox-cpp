function(_def_exec mainFile std_suffix)
    cmake_parse_arguments(
        PARSE_ARGV 2
        exec
        ""
        ""
        "DEFINITIONS"
    )
    get_filename_component(binaryName ${mainFile} NAME_WE)
    add_executable(${binaryName} ${mainFile})
    target_compile_definitions(${binaryName} PRIVATE ${exec_DEFINITIONS})
    target_compile_features(
        ${binaryName}
        PRIVATE
        cxx_std_${std_suffix}
    )
    set_target_properties(
        ${binaryName}
        PROPERTIES
        CXX_EXTENSIONS OFF
    )
endfunction()

function(def_exec11 mainFile)
    _def_exec(${mainFile} 11 ${ARGV})
endfunction()

function(def_exec14 mainFile)
    _def_exec(${mainFile} 14 ${ARGV})
endfunction()

function(def_exec17 mainFile)
    _def_exec(${mainFile} 17 ${ARGV})
endfunction()
