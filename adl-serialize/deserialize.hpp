// This file extends ideas from serialize method. Read that file first.
#pragma once
#include <string>
#include <type_traits>

namespace custom {
namespace detail {

template <class T> class deserialize_tag {};

template <class T> std::enable_if_t<std::is_enum<T>::value, T> deserialize(const std::string& in, deserialize_tag<T>) {
    return static_cast<T>(std::stoi(in));
}

template <class T> T doDeserialize(const std::string& in) {
    // function overloading on return type only is impossible, thus additional
    // tag class is introduced for this purpose only; tag is last to allow for
    // user-side default value and explicit testing
    return deserialize(in, deserialize_tag<T>{});
}

}  // namespace detail

template <class T> inline T deserialize(const std::string& in) {
    return detail::doDeserialize<T>(in);
}

using detail::deserialize_tag;

}  // namespace custom
