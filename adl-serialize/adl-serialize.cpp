#include <iostream>
#include <sstream>
#include "deserialize.hpp"
#include "serialize.hpp"

namespace user {

struct Person {
    std::string name;
    int age;
};

std::string serialize(const Person& person) {
    return person.name + " " + std::to_string(person.age) + "\n";
}

Person deserialize(const std::string& data, custom::deserialize_tag<Person> = {}) {
    std::istringstream is(data);
    Person out;
    is >> out.name >> out.age;
    return out;
}

struct Nonserializable {};

} // namespace user

std::string testSerialize(user::Person person) {
    const auto data = custom::serialize(person);
    std::cout << data;
    #ifdef NOSERIALIZE
    std::cout << custom::serialize(Nonserializable{});
    #endif
    return data;
}

void testDeserialize(const std::string& data) {
    const auto person = custom::deserialize<user::Person>(data);
    std::cout << person.name << " (" << person.age << ")\n";
}

int main(int, char**) {
    testDeserialize(testSerialize({"John", 33}));
    return 0;
}
