#pragma once
#include <string>

namespace custom {
namespace detail {

// overload for enums & enum classes
template <class T> std::enable_if_t<std::is_enum<T>::value, std::string> serialize(const T& in) {
    return std::to_string(static_cast<int>(in));
}

// using SFINAE for clean message in case of T conversion function is not declared
template <class T> inline auto doSerialize(const T& obj) -> decltype(serialize(obj)) {
    // using ADL for function selection, unqualifed name is important
    return serialize(obj);
}

}  // namespace detail

/// Serializes object of type T to 'custom' format.
template <class T> inline std::string serialize(const T& obj) {
    // detail::doSerialize is introduced only so that user defined converters
    // may be named 'serialize' (avoiding recurring function call).
    return detail::doSerialize(obj);
}

}  // namespace custom
