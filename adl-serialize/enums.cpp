#include <iostream>

#include "serialize.hpp"
#include "deserialize.hpp"

namespace user {

// these enums serialize to ints
enum class Foo { A = 0, B = 3, C = 9 };
enum Bar { Bar_Unknown = -10, Bar_Other = 10 };

// this enum uses complex serialization to string
enum class Complex { None = 0, Set = 1 };

std::string serialize(const Complex& in) {
    switch (in) {
        case Complex::Set: return "Set";
        default: return "None";
    }
}

Complex deserialize(const std::string& in, custom::deserialize_tag<Complex> = {}) {
    return in == "Set" ? Complex::Set : Complex::None;
}

}  // namespace user

int main(int, char**) {
    using namespace ::custom;
    std::cout << static_cast<int>(deserialize<user::Foo>(serialize(user::Foo::B))) << "\n";
    std::cout << static_cast<int>(deserialize<user::Bar>(serialize(user::Bar_Unknown))) << "\n";
//    std::cout << static_cast<int>(deserialize<user::Bar>(serialize(3))) << "\n";  // this will not compile
//    std::cout << deserialize<int>(serialize(user::Foo::C)) << "\n";  // this will not compile
    std::cout << static_cast<int>(deserialize<user::Complex>("Set")) << "\n";
    std::cout << static_cast<int>(deserialize<user::Complex>("None")) << "\n";
    std::cout << static_cast<int>(deserialize<user::Complex>(serialize(static_cast<user::Complex>(666)))) << "\n";
    return 0;
}
