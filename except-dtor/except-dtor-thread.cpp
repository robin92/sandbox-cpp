#include <thread>
#include "common.hpp"

int main(int argc, char**) {
    std::unique_ptr<std::thread> t;
    switch (argc) {
        case 1: t = std::make_unique<std::thread>(fn); break;
        default: t = std::make_unique<std::thread>(gn); break;
    }
    t->join();
    return 0;
}
