#pragma once
#include <iostream>

struct Foo {
    Foo() { std::cout << "ctor\n"; }
    ~Foo() { std::cout << "dtor\n"; }
};

void func() {
    Foo foo;
    throw std::runtime_error("");
}

void fn() {
    func();
}

void gn() {
    try {
        func();
    }
    catch (...) {
        throw;
    }
}
