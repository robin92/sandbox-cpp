// copy ctor, copy assign and copy initialization features
#include <iostream>

struct Foo {
    Foo() { std::cout << "default ctor\n"; }
    Foo(const Foo&) { std::cout << "copy ctor\n"; }
    ~Foo() { std::cout << "dtor\n"; }
    
    Foo& operator= (const Foo&) { std::cout << "copy assign\n"; return *this; }
};

int main(int, char**) {
    Foo a;
    Foo b(a);
    Foo c = b;  // copy ctor! (copy initialization feature)
    a = c;      // both left and right exist -> copy assign
    a = Foo(a); // ?
    return 0;
}
