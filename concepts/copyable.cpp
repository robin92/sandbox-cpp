#include <type_traits>

template <class T>
concept Copyable = std::is_copy_constructible_v<T>;

template <Copyable T>
constexpr auto accept(const T&) { return true; }

template <class T>
constexpr auto accept(const T&) { return false; }

struct noncopyable {
    constexpr noncopyable() noexcept {}
    constexpr noncopyable(noncopyable&&) noexcept {}
    constexpr ~noncopyable() {}

    constexpr noncopyable& operator=(noncopyable&&) noexcept { return *this; }

    noncopyable(const noncopyable&) = delete;
    noncopyable& operator=(const noncopyable&) = delete;
};

struct foo {};
struct bar : noncopyable {};

int main(int, char**) {
    static_assert(accept(foo{}));
    static_assert(not accept(bar{}));
    return 0;
}
