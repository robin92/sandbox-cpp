#include <cassert>
#include <string>
#include <vector>

struct Foo {
    std::vector<char> data;
    
    template <int... Pack> void call(std::string str) {
        data = {str[Pack]...};
    }
};

int main(int, char**) {
    Foo f;
    f.call<0, 5, 1, 4, 2, 3>("abcdef");
    assert(f.data == (std::vector<char>{'a', 'f', 'b', 'e', 'c', 'd'}));
    return 0;
}
