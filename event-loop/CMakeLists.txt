find_package(Threads REQUIRED)

link_libraries(Threads::Threads)

def_exec14(condition-variable.cpp)
def_exec17(
    eventloop2.cpp
    DEFINITIONS
        WORKERS=12
)
