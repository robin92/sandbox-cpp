#include <atomic>
#include <condition_variable>
#include <functional>
#include <iostream>
#include <mutex>
#include <thread>
#include <queue>

using Callback = std::function<void()>;

class EventLoop
{
    std::condition_variable _cv;
    std::mutex _mutex;
    std::queue<Callback> _callbacks;
    std::atomic_bool _active;
    
public:
    void run();
    void stop();

    void push(Callback);
};

void EventLoop::run()
{
    std::cerr << "event loop " << this << " running on thread " << std::this_thread::get_id() << "\n";
    _active = true;
    while (_active)
    {
        std::unique_lock<decltype(_mutex)> lock(_mutex);
        _cv.wait(lock, [this] { return not _callbacks.empty(); });

        while (not _callbacks.empty())
        {
            auto callback = _callbacks.front();
            _callbacks.pop();
            lock.unlock();
            callback();
            lock.lock();
        }
    }
    std::cerr << "event loop " << this << " stopped\n";
}

void EventLoop::stop()
{
    _active = false;
}

void EventLoop::push(Callback callback)
{
    std::lock_guard<decltype(_mutex)> lock(_mutex);
    _callbacks.push(std::move(callback));
    _cv.notify_one();
}

int main(int, char**)
{
    EventLoop eventLoop;
    auto loopThread = std::thread([&] { eventLoop.run(); });
    auto& is = std::cin;
    while (not is.eof())
    {
        std::string line;
        std::getline(is, line);
        eventLoop.push([=] { std::cout << line << "\n"; });
    }
    eventLoop.stop();
    loopThread.join(); // running on thread for reducing cpu load
    return 0;
}
