#ifndef WORKERS
#define WORKERS 0
#endif

#include <atomic>
#include <condition_variable>
#include <functional>
#include <future>
#include <iostream>
#include <mutex>
#include <queue>
#include <sstream>
#include <string>
#include <string_view>
#include <variant>


const thread_local std::string tid = ([]{
        std::ostringstream oss;
        oss << "[" << std::hex << std::this_thread::get_id() << "]";
        return oss.str();
    })();
static std::mutex coutMtx;


template <class... Args> void log(Args... args) {
    std::lock_guard lock{coutMtx};
    ((std::cout << tid << " ") << ... << args) << "\n";
}

template <class... Args> std::string prompt(std::istream& is, Args... args) {
    std::lock_guard lock{coutMtx};
    ((std::cout << tid << " ") << ... << args);
    std::string line;
    std::getline(is, line);
    return line;
}

template <class Queue, class T = typename Queue::value_type> T take(Queue& queue) {
    auto elem = queue.front();
    queue.pop();
    return elem;
}

auto Not(auto&& matcher) -> decltype(auto) {
    return [=] { return not(matcher()); };
}

auto Ge(auto&& lhs, auto&& rhs) -> decltype(auto) {
    return [&] { return lhs >= rhs; };
}

auto Empty(auto&& storage) -> decltype(auto) {
    return [&] { return storage.empty(); };
}


template <size_t N> class ThreadPolicy {
    std::mutex _mtx;
    std::condition_variable _cv;
    std::atomic<size_t> _readyCount = 0;

public:
    void run(std::function<void()> fn) {
        std::array<std::future<void>, N> workers;
        for (auto&& w : workers) {
            w = std::async(&ThreadPolicy::spawn, this, fn);
        }
    }

private:
    void spawn(std::function<void()> fn) {
        log("worker setting up");
        waitUntilAllReady();
        fn();
    }

    void waitUntilAllReady() {
        std::unique_lock lock{_mtx};
        _readyCount++;
        while (not _cv.wait_for(lock, std::chrono::milliseconds(100), Ge(_readyCount, N)));
    }
};

template <> class ThreadPolicy<0> {
public:
    void run(std::function<void()> fn) {
        fn();
    }
};

using ThisThreadPolicy = ThreadPolicy<0>;

template <class Policy, class T> class EventLoop {
    using Handler = std::function<void(T)>;

    Policy _policy;
    std::mutex _handlerMtx;
    Handler _handler = nullptr;
    std::atomic_bool _run = false;
    std::mutex _tasksMtx;
    std::condition_variable _tasksCv;
    std::condition_variable _emptyCv;
    std::queue<T> _tasks;

public:
    using event_type = T;

    /// Connects a task handling function to an event.
    void connect(Handler);

    /// Inserts a task on execution queue.
    void push(T&&);

    /// Runs event loop.
    void run();

    /// Blocks until all tasks have been executed.
    void wait();

    /// Stops event loop, does not clear execution queue.
    void stop();

private:
    std::optional<T> take();
    void invoke(std::optional<T>);
};

template <class P, class T> void EventLoop<P, T>::connect(Handler h) {
    std::lock_guard lock{_handlerMtx};
    _handler = h;
}

template <class P, class T> void EventLoop<P, T>::push(T&& t) {
    std::lock_guard lock{_tasksMtx};
    _tasks.push(std::move(t));
    _tasksCv.notify_one();
}

template <class P, class T> void EventLoop<P, T>::run() {
    _run = true;
    _policy.run([this] {
        while (_run) {
            invoke(take());
        }
    });
}

template <class P, class T> std::optional<T> EventLoop<P, T>::take() {
    constexpr auto timeout = std::chrono::milliseconds(100);
    std::unique_lock lock{_tasksMtx};
    if (not _tasksCv.wait_for(lock, timeout, Not(Empty(_tasks)))) {
        _emptyCv.notify_all();
        return {};
    }
    return ::take(_tasks);
}

template <class P, class T> void EventLoop<P, T>::invoke(std::optional<T> task) {
    std::lock_guard lock{_handlerMtx};
    if (task and _handler) {
        std::invoke(_handler, *task);
    }
}

template <class P, class T> void EventLoop<P, T>::wait() {
    constexpr auto timeout = std::chrono::milliseconds(100);
    while (true) {
        std::unique_lock lock{_tasksMtx};
        if (_emptyCv.wait_for(lock, timeout, Empty(_tasks))) {
            return;
        }
    }
}

template <class P, class T> void EventLoop<P, T>::stop() {
    _run = false;
    _tasksCv.notify_one();
}


template <class Policy, class... Ts> class MixedEventLoop :
    public EventLoop<Policy, std::variant<Ts...>> {
public:
    template <class Handler> void connect(Handler handler) {
        EventLoop<Policy, std::variant<Ts...>>::connect([=](auto&& v) { std::visit(handler, v); });
    }
};


namespace app {

struct Ask {};

struct Say {
    std::string value;
};

struct Quit {};

using EventLoop = MixedEventLoop<ThreadPolicy<WORKERS>, Ask, Say, Quit>;

EventLoop::event_type mkcmd(std::string_view cmd) {
    if (cmd == "/quit") return Quit{};
    return Say{std::string{cmd.data()}};
}

} // namespace app

// wrapper for making multiple lambdas work with std::visit, see
// https://en.cppreference.com/w/cpp/language/class_template_argument_deduction#User-defined_deduction_guides
// for details on second line
template <class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template <class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

int main(int, char**) {
    using namespace ::app;
    log("starting");
    app::EventLoop loop;
    loop.connect(overloaded {
        [&](Ask) { 
            auto& is = std::cin;
            loop.push(is.eof() ? Quit{} : mkcmd(prompt(is, "?> ")));
        },
        [&](Say c) {
            log(c.value);
            loop.push(Ask{});
        },
        [&](Quit) { loop.stop(); },
    });
    loop.push(Ask{});
    loop.run();
    return 0;
}
