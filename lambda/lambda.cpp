#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

template <class T> std::ostream& operator<< (std::ostream& os, const std::vector<T>& elements) {
    std::copy(elements.begin(), elements.end(), std::ostream_iterator<T>(os, " "));
    return os;
}

void lambda_mutable() {
    std::vector<int> elements(10);
    std::generate(elements.begin(), elements.end(), [n = 0]() mutable { return n++; });
    std::cout << elements << "\n";
}

int main(int, char**) {
    lambda_mutable();
    return 0;
}
